from django.db import models



class Teachers(models.Model):
    image = models.ImageField(default='no_image.jpg', upload_to='images')
    full_name = models.CharField(max_length=50)
    description = models.CharField(max_length=50) 

    def __str__(self):
        return self.full_name