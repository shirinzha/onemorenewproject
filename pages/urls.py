from django.urls import path
from .views import HomePageView, TeacherCreateView

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("new_teacher/", TeacherCreateView.as_view(), name="new_teacher"),
]