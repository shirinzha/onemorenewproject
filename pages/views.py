from django.shortcuts import render
from django.views.generic import TemplateView, CreateView
from .models import Teachers
from django.urls import reverse_lazy

class HomePageView(TemplateView):
    template_name = 'home.html'

class TeacherCreateView(CreateView):
    model = Teachers
    fields = '__all__'
    template_name = 'create_teacher.html'
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['teachers'] = Teachers.objects.all()
        return context
